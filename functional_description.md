# Functional description

## tickets specification catalog

Tickets provides a Catalog function for tickets specification

Designer will be able to:

- Declare a new ticket model
- Consult ticket models
- Modify ticket model
- Delete ticket model



### tickets management

Tickets provides ticket API to allow :

- Declare a new ticket instance
- Consult ticket intance
- Modify ticket instance
- Delete ticket instance


### Demo portal

Tickets provides a demo portal to allow :

view tickets instances and related tickets template
