#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""tickets related operations
"""
import os
import logging
import yaml
import tickets_exceptions as sr_exceptions
from tickets_client_to_api import get_tickets_spec
from tickets_utils import get_config

LOGGER = logging.getLogger('tickets_api')
if LOGGER.hasHandlers():
    # Logger is already configured, remove all handlers
    LOGGER.handlers = []
SIMUL = get_config("simulator_mode")

def check_tickets(tickets):
    """check if tickets composition is valid
    """
    return tickets

