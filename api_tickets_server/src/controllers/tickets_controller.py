"""tickets related operations
"""
import uuid
import logging
import json
# import os
# import json
# import jsonschema
import tickets_exceptions as tickets_exceptions
from tickets_handling import check_tickets
from tickets_mongo_client import (mongo_find,
                                  mongo_find_one,
                                  mongo_find_one_and_update,
                                  mongo_delete)
from tickets_utils import get_config

LOGGER = logging.getLogger('tickets_api')
SIMUL = get_config("simulator_mode")


def ticketsCreate(tickets):  # pylint: disable=C0103
    """create ticket
    """
    print('Coucou  ',tickets)
    LOGGER.debug('call to ticketsCreate function')
    LOGGER.debug('\ndata received (converted into Json) :\n%s\n',
                 json.dumps(tickets, indent=4, sort_keys=True))
    LOGGER.debug('data type :  %s', type(tickets))

    tickets_instance = tickets
    tickets_instance["@type"] = "tickets"
    tickets_instance["@baseType"] = "tickets"
    tickets_instance["@schemaLocation"] = ""
    tickets_instance["category"] = "cfs"
    query = {'ticketsId': tickets_instance["ticketsId"]}
    try:
        resp = mongo_find_one(query)
    except tickets_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is not None:
        message_value = ("tickets : " +
                         tickets_instance["ticketsId"] +
                         " already exists")
        message = {"message": message_value}
        LOGGER.debug('message : %s ', message_value)
        return message, 409
    try:
        tickets_instance = check_tickets(tickets_instance)
    except (tickets_exceptions.TicketsException,
            tickets_exceptions.TicketsNotFoundException) as error:
        LOGGER.error("problem : %s", error.args[0])
        message = {"problem": error.args[0]}
        return message, 503
    tickets_instance['ticketsId'] = str(uuid.uuid4())
    tickets_instance["href"] = "/tickets/" + tickets_instance['ticketsId']
    query = {'id': tickets_instance['ticketsId']}
    try:
        resp = mongo_find_one_and_update(query, tickets_instance, True)
    except tickets_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    return tickets_instance, 201


def ticketsDelete(ticketsId):  # pylint:disable=C0103
    """delete ticket
    """
    LOGGER.debug('call to ticketsDelete function')
    LOGGER.debug('data received :  %s', ticketsId)

    query = {'id': ticketsId}
    try:
        resp = mongo_delete(query)
    except tickets_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is None:
        LOGGER.debug('response is 404 not found')
        return resp, 404
    return {}, 204


def ticketsFind():  # pylint: disable=C0103
    """find tickets
    """
    LOGGER.debug('call to ticketsFind function')

    query = {}
    try:
        resp = mongo_find(query)
    except tickets_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    return resp, 200


def ticketsGet(ticketsId):  # pylint: disable=C0103
    """get tickets
    """
    LOGGER.debug('call to ticketsGet function')
    LOGGER.debug('data received :  %s', ticketsId)

    query = {'id': ticketsId}
    try:
        resp = mongo_find_one(query)
    except tickets_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is None:
        LOGGER.debug('response is 404 not found')
        return {}, 404
    return resp, 200


def ticketsUpdate(ticketsId,  # pylint: disable=C0103
                               tickets):
    """update ticket
    """
    LOGGER.debug('call to ticketsUpdate function')
    LOGGER.debug('\ndata received (converted into Json) :\n%s\n',
                 json.dumps(tickets, indent=4, sort_keys=True))
    LOGGER.debug('data type :  %s', type(tickets))
    LOGGER.debug('data received :  %s', ticketsId)
    LOGGER.debug('data type :  %s', type(ticketsId))

    query = {'id': ticketsId}
    try:
        resp = mongo_find_one_and_update(query, tickets, False)
    except tickets_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is None:
        LOGGER.debug('response is 404 not found')
        return {}, 404
    return resp, 200
