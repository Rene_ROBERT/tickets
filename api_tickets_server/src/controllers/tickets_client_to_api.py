#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""clients to other API
"""
import logging
import requests
import tickets_exceptions as tickets_exceptions
from tickets_utils import get_config

LOGGER = logging.getLogger('tickets_api')
if LOGGER.hasHandlers():
    # Logger is already configured, remove all handlers
    LOGGER.handlers = []
PORT = str(get_config("tickets.tickets_spec_api.port"))
TICKETS_SPEC_API_BASE_URL = ("http://" +
                         get_config("tickets.tickets_spec_api.host") +
                         ":" +
                         PORT +
                         get_config("tickets.tickets_spec_api.url"))
TICKETS_SPEC_API_HEADER = get_config("tickets.tickets_spec_api.headers")


def get_tickets_spec(tickets_spec):
    ''' get a tickets_spec by id
    '''
    LOGGER.debug('call to get_tickets_spec function')

    url = TICKETS_SPEC_API_BASE_URL + '/ticketsSpecification/' + tickets_spec["id"]
    proxies = {}

    LOGGER.debug('url used : %s ', url)
    LOGGER.debug('proxy used : %s ', proxies)
    try:
        response = requests.request("GET",
                                    url,
                                    headers=TICKETS_SPEC_API_HEADER,
                                    proxies=proxies)
    except Exception:
        message = "communication problem with TICKETS SPEC API"
        LOGGER.error(message)
        raise tickets_exceptions.TicketsSpecException(message)
    if response.status_code == 404:
        message = (tickets_spec["name"] +
                   " with id " +
                   tickets_spec["id"] +
                   " not found in tickets spec API")
        LOGGER.error(message)
        raise tickets_exceptions.TicketsSpecNotFoundException(message)
    response = response.json()
    return response
