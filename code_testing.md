# Testing the code

Each API server has a tox.ini file that contains a list
of tools to be run in order to test the code quality

pylint, pep8, pytest, test coverage will be used

just run:

```bash
tox
```
