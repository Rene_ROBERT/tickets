#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""tests cfs_spec_handling
"""
import re
import json
import yaml
import pytest
# from cfs_spec_handling import check_cfs_spec, generate_tosca


# @pytest.mark.parametrize("status_code, distributionStatus, expected_rfs_list_validated, expected_bad_rfs_message_file", [
#     (200, "DISTRIBUTED", True, "expected_bad_rfs_message1.json"),
#     (200, "fake", True, "expected_bad_rfs_message3.json"),
#     (400, "fake", False, "expected_bad_rfs_message2.json")
# ])
# def test_check_cfs_spec(requests_mock, status_code, distributionStatus, expected_rfs_list_validated, expected_bad_rfs_message_file):
#     """test
#     """
#     test_data_path = "src/test/data/"

#     filename = "vbox_cfs_spec.json"
#     with open(test_data_path + filename) as file:
#         service_spec = json.load(file)

#     response = {}
#     response["distributionStatus"] = distributionStatus

#     filename = expected_bad_rfs_message_file
#     with open(test_data_path + filename) as file:
#         expected_bad_rfs_message = json.load(file)

#     matcher = re.compile('/rfsSpecification')
#     requests_mock.get(matcher, status_code=status_code, json=response)
#     rfs_list_validated, bad_rfs_message = check_cfs_spec(service_spec)
#     assert rfs_list_validated == expected_rfs_list_validated
#     assert bad_rfs_message == expected_bad_rfs_message


# def test_generate_tosca():
#     """test
#     """
#     test_data_path = "src/test/data/"

#     filename = "vbox_cfs_spec.json"
#     with open(test_data_path + filename) as file:
#         service_spec = json.load(file)
#     generate_tosca(service_spec)
#     directory = 'src/tosca/'
#     filename = 'cfs-' + service_spec["name"] + '-template.yml'
#     with open(directory + filename, 'r') as file:
#         expected_result = yaml.safe_load(file)
#     assert service_spec == expected_result
