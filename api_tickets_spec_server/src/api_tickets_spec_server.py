#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""tickets specification api
"""

from logging.config import dictConfig
import os
import connexion
from tickets_spec_logdict import conf_dict
from tickets_spec_utils import get_config

# logging setup
CONF = conf_dict('tickets_spec_api.log')
dictConfig(CONF)

TICKETS_SPEC_API_SERVER = get_config("tickets.tickets_spec_api.server")
TICKETS_SPEC_API_HOST = os.environ['tickets_spec_server_hostname']
TICKETS_SPEC_API_PORT = os.environ['tickets_spec_server_port']
TICKETS_SPEC_API_DEBUG_MODE = get_config("tickets.tickets_spec_api.api_debug_mode")

API_TICKETS_SPEC_SERVER = connexion.App(__name__,
                                        specification_dir='openapi/')
API_TICKETS_SPEC_SERVER.add_api('swagger.yaml',
                                strict_validation=True,
                                validate_responses=True)
API_TICKETS_SPEC_SERVER.run(server=TICKETS_SPEC_API_SERVER,
                            host=TICKETS_SPEC_API_HOST,
                            port=TICKETS_SPEC_API_PORT,
                            debug=TICKETS_SPEC_API_DEBUG_MODE)
