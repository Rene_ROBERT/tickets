#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
""" Module to define testing exceptions """

__author__ = ("Morgan Richomme <morgan.richomme@orange.com>")


class MongodbException(Exception):
    """Problem with mongodb communication"""


class TicketsSpecException(Exception):
    """Communication problem with tickets specification api"""


class TicketsSpecNotFoundException(Exception):
    """tickets specification does not exist"""
