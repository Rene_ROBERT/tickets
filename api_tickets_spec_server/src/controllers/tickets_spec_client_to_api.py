#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# """clients to other API
# """
# import logging
# import requests
# import tickets_spec_exceptions as tickets_spec_exceptions
# from tickets_spec_utils import get_config

# LOGGER = logging.getLogger('tickets_spec_api')
# if LOGGER.hasHandlers():
#     # Logger is already configured, remove all handlers
#     LOGGER.handlers = []
