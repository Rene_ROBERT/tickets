"""tickets related operations
"""
import uuid
import logging
import json
# import os
# import json
# import jsonschema
import tickets_spec_exceptions as tickets_spec_exceptions
from tickets_spec_handling import check_tickets_spec
from tickets_spec_mongo_client import (mongo_find,
                                       mongo_find_one,
                                       mongo_find_one_and_update,
                                       mongo_delete)
from tickets_spec_utils import get_config

LOGGER = logging.getLogger('tickets_spec_api')
SIMUL = get_config("simulator_mode")


def ticketsSpecCreate(ticketsSpec):  # pylint: disable=C0103
    """create ticket specification
    """
    print('Coucou  ',ticketsSpec)
    LOGGER.debug('call to ticketsSpecCreate function')
    LOGGER.debug('\ndata received (converted into Json) :\n%s\n',
                 json.dumps(ticketsSpec, indent=4, sort_keys=True))
    LOGGER.debug('data type :  %s', type(ticketsSpec))

    tickets_spec = ticketsSpec
    tickets_spec["@type"] = "ticketsSpec"
    tickets_spec["@baseType"] = "ticketsSpec"
    tickets_spec["@schemaLocation"] = ""
    tickets_spec["category"] = "ticketsSpec"
    query = {'id': tickets_spec["ticketsSpecId"]}
    try:
        resp = mongo_find_one(query)
    except tickets_spec_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is not None:
        message_value = ("ticketsSpec : " +
                         tickets_spec["ticketsSpecId"] +
                         " already exists")
        message = {"message": message_value}
        LOGGER.debug('message : %s ', message_value)
        return message, 409
    try:
        tickets_spec = check_tickets_spec(tickets_spec)
    except (tickets_spec_exceptions.TicketsSpecException,
            tickets_spec_exceptions.TicketsSpecNotFoundException) as error:
        LOGGER.error("problem : %s", error.args[0])
        message = {"problem": error.args[0]}
        return message, 503
    tickets_spec['ticketsSpecId'] = str(uuid.uuid4())
    tickets_spec["href"] = "/ticketsSpec/" + tickets_spec['ticketsSpecId']
    query = {'id': tickets_spec['ticketsSpecId']}
    try:
        resp = mongo_find_one_and_update(query, tickets_spec, True)
    except tickets_spec_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    return tickets_spec, 201


def ticketsSpecDelete(ticketsSpecId):  # pylint:disable=C0103
    """delete ticket
    """
    LOGGER.debug('call to ticketsDelete function')
    LOGGER.debug('data received :  %s', ticketsSpecId)

    query = {'id': ticketsSpecId}
    try:
        resp = mongo_delete(query)
    except tickets_spec_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is None:
        LOGGER.debug('response is 404 not found')
        return resp, 404
    return {}, 204


def ticketsSpecFind():  # pylint: disable=C0103
    """find tickets
    """
    print('Coucou, je suis ticketsSpecFind  ')
    LOGGER.debug('call to ticketsFind function')

    query = {}
    try:
        resp = mongo_find(query)
    except tickets_spec_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    return resp, 200


def ticketsSpecGet(ticketsSpecId):  # pylint: disable=C0103
    """get tickets
    """
    print('Coucou, je suis ticketsSpecGet  ',ticketsSpecId )

    LOGGER.debug('call to ticketsSpecGet function')
    LOGGER.debug('data received :  %s', ticketsSpecId)

    query = {'id': ticketsSpecId}
    try:
        resp = mongo_find_one(query)
    except tickets_spec_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is None:
        LOGGER.debug('response is 404 not found')
        return {}, 404
    return resp, 200


def ticketsSpecUpdate(ticketsSpecId,  # pylint: disable=C0103
                  ticketsSpec):   # pylint: disable=C0103
    """update ticket specification
    """
    LOGGER.debug('call to ticketsSpecUpdate function')
    LOGGER.debug('\ndata received (converted into Json) :\n%s\n',
                 json.dumps(ticketsSpec, indent=4, sort_keys=True))
    LOGGER.debug('data type :  %s', type(ticketsSpec))
    LOGGER.debug('data received :  %s', ticketsSpecId)
    LOGGER.debug('data type :  %s', type(ticketsSpecId))

    query = {'id': ticketsSpecId}
    try:
        resp = mongo_find_one_and_update(query, ticketsSpec, False)
    except tickets_spec_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is None:
        LOGGER.debug('response is 404 not found')
        return {}, 404
    return resp, 200
