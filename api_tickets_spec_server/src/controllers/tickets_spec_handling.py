#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""tickets specification related operations
"""
import os
import logging
import yaml
import tickets_spec_exceptions as tickets_spec_exceptions
# from tickets_spec_client_to_api import get_tickets_spec
from tickets_spec_utils import get_config

LOGGER = logging.getLogger('tickets_spec_api')
if LOGGER.hasHandlers():
    # Logger is already configured, remove all handlers
    LOGGER.handlers = []
SIMUL = get_config("simulator_mode")

def check_tickets_spec(tickets_spec):
    """check if tickets Spec composition is valid
    """
    return tickets_spec
