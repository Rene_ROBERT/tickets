# Tickets Specification API server

## Overview

This API server is about managing Tickets specification.

## Requirements

## configuration file

Configuration file is config_default.yaml

located in api_tickets_spec_server/src/app_conf

It contains mainly information about

* loglevel
* simulator mode
* mongodb API information


## Running via python3

To run the server, please execute the following:

```bash
pip3 install -r requirements.txt
cd src
python3 api_tickets_spec_server.py
```

## Running with Docker

To run the server on a Docker container, please execute the following:

```bash
# building the image
docker build -t api_tickets_spec_server .

# starting up the container
# need /data repository on the hosts
docker run -p 6011:6011 --volume=/data:/data api_tickets_spec_server
```

## Use

open your browser to here:

```bash
http://localhost:6011/tickets/ticketsSpecManagement/api/v1/ui/
```

Your Swagger definition lives here:

```bash
http://localhost:6011/tickets/ticketsSpecManagement/api/v1/swagger.json
```
