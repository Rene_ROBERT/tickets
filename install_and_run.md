# Running with docker-compose

pre-requisite : linux, docker, docker compose

 ```bash
 cd
 mkdir projects
 cd ~/projects
 git clone https://gitlab.tech.orange/rene.robert/tickets.git
 cd tickets
 docker-compose build
```

if your are behind a proxy you may have to use that kind of command (replace your_proxy)

```bash
docker-compose build --build-arg HTTP_PROXY=<your_proxy> --build-arg \
HTTPS_PROXY=<your_proxy> api_tickets_spec_server \
api_tickets_server portal
docker-compose up
```

all docker images will be built and containers launched

to stop the containers :

docker-compose down

or

docker-compose down -v

the "-v" option will remove the volumes (all stored data are lost)

## Testing all APIs

To check Tickets is correctly working, some tests can be runs:
To be completed

## Configure

To be completed

## Explore APIs with swaggerUI

Open a web browser and go to

```bash
http://localhost:6011/tickets/ticketsSpecManagement/api/v1/ui/
```

you should see the swagger UI explorer for API TicketsSpecAPI

Open an other tab in your browser and go to

```bash
http://localhost:6010/tickets/ticketsManagement/api/v1/ui/
```

you should see the swagger UI explorer for API ticketsAPI


## Graphical Visualisation of service instance

To be completed

you should see the demo portal display
